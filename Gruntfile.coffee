global.Handlebars = require 'handlebars'
path = require 'path'
module.exports = (grunt) ->
  grunt.initConfig
    pkg: grunt.file.readJSON 'package.json'
    less:
      build:
        files:
          'dist/style.css': 'styles/*'
    copy:
      build:
        files: [
          {expand: true, cwd: 'html', src: ['**'], dest: 'dist/'}
          # Handles a small bug in the debugger needing the source map
          {expand: true, cwd: 'bower_components/jquery/dist', src: ['jquery.min.map'], dest: 'dist/'}
        ]
    compile_templates:
      build:
        files: [
          src: ['templates/*.hbs']
          dest: 'dist/templates.js'
        ]
    rendr_stitch:
      options:
        dependencies: [
          'bower_components/jquery/dist/jquery.min.js'
          'bower_components/bootstrap/dist/js/bootstrap.min.js'
          'bower_components/handlebars/handlebars.runtime.js'
          'bower_components/underscore/underscore.js'
          'bower_components/backbone/backbone.js'
          'dist/templates.js'
        ]
      files:
        src: 'scripts/**/*.coffee'
        dest: 'dist/app.js'
    watch:
      css:
        files: [
          'styles/*.less'
        ]
        tasks: ['less']
      js:
        files: [
          'scripts/**/*.coffee'
          'scripts/**/*.js'
        ]
        tasks: ['rendr_stitch']
      templates:
        files: [
          'templates/**/*.hbs'
        ]
        tasks: ['compile_templates', 'rendr_stitch']
      html:
        files: [
          'html/*'
        ]
        tasks: ['copy']


  grunt.loadNpmTasks 'grunt-rendr-stitch'
  grunt.loadNpmTasks 'grunt-contrib-less'
  grunt.loadNpmTasks 'grunt-contrib-copy'
  grunt.loadNpmTasks 'grunt-contrib-watch'

  # Compile the templates into a javascript object for use in compiling the
  # the final javascript file.
  grunt.registerMultiTask 'compile_templates', 'Compile Handlebars templates.', ->
    options = knownHelpersOnly: false
    file = @files[0]

    output = [
      'Handlebars.templates = Handlebars.templates || {};'
      'var template = Handlebars.template;'
    ]

    for filename in file.src
      template = grunt.file.read filename
      output.push "Handlebars.templates['#{path.basename filename, path.extname filename}'] = template(#{Handlebars.precompile template, options});"

    output.push 'Handlebars.partials = Handlebars.templates;'
    console.log 
    grunt.file.write file.dest, output.join '\n'
    console.log "File #{file.dest} created."

  grunt.registerTask 'build', ['compile_templates', 'copy', 'less', 'rendr_stitch']
  grunt.registerTask 'default', ['watch']