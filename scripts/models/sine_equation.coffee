# Model for a sine equation modeled by the following:
# y = a * sin(p(x - h)) + k
module.exports = class SineEquation extends Backbone.Model
  defaults:
    h: 0
    k: 0
    a: 1
    p: 1

  evaluate: (x) ->
    @get('a') * Math.sin(@get('p') * x - @get('h')) + @get('k')

  amplitude: ->
    Math.abs @get 'a'

  period: ->
    2 * Math.PI / @get 'p'
