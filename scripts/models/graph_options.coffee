# Model for the graph window options for a graph.
module.exports = class GraphOptions extends Backbone.Model
  defaults:
    minX: -10
    maxX: 10
    minY: -2
    maxY: 2

  rangeX: ->
    @get('maxX') - @get('minX')

  rangeY: ->
    @get('maxY') - @get('minY')
