module.exports = class Base extends Backbone.View
  initialize: (@options = {}) ->

  renderData: ->
    @model?.toJSON()

  render: ->
    @$el.empty().append Handlebars
      .templates[_.result @, 'template'](@renderData())
    @