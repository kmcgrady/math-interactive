BaseView = require './base'

module.exports = class EquationEditor extends BaseView
  template: 'equation_editor'

  initialize: ->
    @listenTo @model, 'change', (model) ->
      changed = model.changedAttributes()
      # MathJax goes crazy when updating calling update on the
      # entire page. It's best to edit the specific values in place
      # for speed and usability.
      for variable, value of changed
        element = @$el.find("[data-variable=#{variable}]").get()
        mathText = MathJax.Hub.getAllJax(element)[0]
        MathJax.Hub.Queue ['Text', mathText, "#{value}"]
