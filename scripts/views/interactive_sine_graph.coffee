BaseView = require './base'
EquationEditor = require './equation_editor'
GraphOptions = require '../models/graph_options'
GraphView = require './graph'
SineEquation = require '../models/sine_equation'

module.exports = class InteractiveSineGraph extends BaseView
  # We use a delay time to avoid redrawing too often when scrubbing
  # The canvas redraw is rather fast. It's the MathJax updating that's slow.
  @DELAY_TIME = 200
  @PIXELS_PER_UNIT = 100
  template: 'interactive_sine_graph'

  events:
    'mousedown .scrubbable': 'mouseBeginScrubbing'
    'mousemove #interactive-sine-graph': 'mouseScrubVariable'
    'mouseup #interactive-sine-graph': 'endScrubbing'
    'touchstart .scrubbable': 'touchBeginScrubbing'
    'touchmove #interactive-sine-graph': 'touchScrubVariable'
    'touchend #interactive-sine-graph': 'endScrubbing'

  initialize: ->
    super
    @equation = new SineEquation()
    @graphOptions = new GraphOptions()
    @scrubMode = false
    @delayTimeStart = null
    @originalVar = null
    @mouseX = null
    @scrubbingVariable = null
  
  render: ->
    super

    @graphView = new GraphView
      el: '#sine-graph'
      equation: @equation
      model: @graphOptions
    @graphView.render()
    @equationEditor = new EquationEditor
      el: '#sine-equation'
      model: @equation
    @equationEditor.render()
    @

  mouseBeginScrubbing: (ev) ->
    ev.preventDefault()
    @beginScrubbing ev, ev.screenX

  touchBeginScrubbing: (ev) ->
    ev.preventDefault()
    @beginScrubbing ev, ev.originalEvent.touches[0]?.screenX

  beginScrubbing: (ev, x) ->
    # We need to denote we are in the "scrubbing" state as well as save the
    # original value, the starting mouse position, etc.
    @scrubMode = true
    @scrubbingVariable = $(ev.currentTarget).data 'variable'
    @originalVar = @equation.get @scrubbingVariable
    @mouseX = x
    @$el.addClass 'scrubbing-mode-on'

    # Store the current time to avoid updating so quickly after.
    @delayTimeStart = Date.now()

  mouseScrubVariable: (ev) ->
    ev.preventDefault()
    @scrubVariable ev.screenX

  touchScrubVariable: (ev) ->
    ev.preventDefault()
    @scrubVariable ev.originalEvent.changedTouches[0]?.screenX

  scrubVariable: (updatedX) ->
    # Bail if we are not scrubbing or it's been too soon.
    return unless @scrubMode
    return if Date.now() - @delayTimeStart < @constructor.DELAY_TIME

    @delayTimeStart = Date.now()

    # Scale the amount change in X  so that for every PIXELS_PER_UNIT
    # of change, the graph would change by 1 unit.
    difference = (updatedX - @mouseX) / @constructor.PIXELS_PER_UNIT
    changes = {}

    # Ensure the variable change is in the hundreths. This just avoids
    # crazy long numbers in the equation.
    varChange = Math.round((@originalVar + difference) * 100) / 100
    changes[@scrubbingVariable] = varChange

    # Setting will trigger changes to the graph and equation editor views
    @equation.set changes

  endScrubbing: (ev) ->
    ev.preventDefault()
    return unless @scrubMode

    @scrubMode = false
    @delayTimeStart = null
    @originalVar = null
    @mouseX = null
    @scrubbingVariable = null
    @$el.removeClass 'scrubbing-mode-on'
