BaseView = require './base'

module.exports = class Graph extends BaseView
  template: 'graph'

  initialize: ({equation}) ->
    super
    @listenTo equation, 'change', =>
      # We do not want to rerender the screen and waste time
      # removing and readding the canvas. Just clear and redraw.
      ctx = @canvas.getContext '2d'
      ctx.clearRect 0, 0, @canvas.width, @canvas.height
      @draw()

  render: ->
    super
    @draw()
    @

  draw: ->
    @canvas = @$el.children('.graph').get 0
    ctx = @canvas.getContext '2d'
    @renderXAxis ctx
    @renderYAxis ctx
    @renderEquation ctx

  renderXAxis: (ctx) ->
    # Don't bother rendering if the X Axis is not shown in the window.
    if @model.get 'minY' > 0 or @model.get 'maxY' < 0
      return

    centerY = Math.round @canvas.height * @model.get('maxY') / @model.rangeY()
    ctx.save()
    ctx.beginPath()
    ctx.moveTo 0, centerY
    ctx.lineTo @canvas.width, centerY
    ctx.strokeStyle = '#000'
    ctx.lineWidth = 2
    ctx.stroke()
    ctx.restore()

  renderYAxis: (ctx) ->
    # Don't bother rendering if the Y Axis is not shown in the window.
    if @model.get 'minX' > 0 or @model.get 'maxX' < 0
      return

    distanceFromLeft = Math.abs @model.get 'minX'
    centerX = Math.round @canvas.width * distanceFromLeft / @model.rangeX()
    ctx.save()
    ctx.beginPath()
    ctx.moveTo centerX, 0
    ctx.lineTo centerX, @canvas.height
    ctx.strokeStyle = '#000'
    ctx.lineWidth = 2
    ctx.stroke()

    ctx.restore()

  renderEquation: (ctx) ->
    {equation} = @options
    ctx.save()
    ctx.beginPath()
    # We want to make sure to move to the first point rather than draw
    # to the first point. We use this flag to check.
    began = false

    # Scale the x and y based on the Graph Options and the Canvas dimensions
    for pixelX in [0...@canvas.width]
      x = @model.get('minX') + @model.rangeX() * pixelX / @canvas.width
      y = equation.evaluate x
      
      distanceFromTop = @model.get('maxY') - y
      pixelY = Math.round distanceFromTop * @canvas.height / @model.rangeY()

      unless began
        ctx.moveTo pixelX, pixelY
        began = true
      else
        ctx.lineTo pixelX, pixelY
    ctx.strokeStyle = '#00f'
    ctx.lineWidth = 2
    ctx.stroke()
    ctx.restore()

