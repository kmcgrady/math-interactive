To Run:
```
 npm install
 bower install
 grunt build
 ```

 You may run `grunt watch` afterwards, and the build will update with any changes.
 